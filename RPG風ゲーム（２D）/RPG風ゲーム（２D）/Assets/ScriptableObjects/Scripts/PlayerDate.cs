using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/PlayerDate",
    fileName = "PlayerDate")
]

//プレイヤーのデータ
public class PlayerDate : ScriptableObject
{
    [SerializeField] private string playerName; //プレイヤーの名前
    [Range(1,10),SerializeField] private int playerLevel;　　//プレイヤーのレベル
    public Dictionary<int, int> levelExp = new Dictionary<int, int>
    {
        {1, 0},{2, 20},{3, 30},{4, 50},{6, 70},{8, 100},{9, 200},{10,350}
    };
    //レベルアップに必要な経験値 <レベル,経験値>


    [SerializeField] private int playerCurrentExp;  // 今の経験値
    [SerializeField] private int totalExp;  //今までの経験値の合計

    public string Name
    {
        set
        {
            playerName = value;
        }
        get
        {
            return playerName;
        }
    }
    public int Level
    {
        set
        {
            playerLevel = value;
        }
        get
        {
            return playerLevel;
        }
    }

    public int CurrentExp
    {
        set
        {
            playerCurrentExp = value;
        }
        get
        {
            return playerCurrentExp;
        }
    }

    public int TotalExp
    {
        set
        {
            totalExp = value;
        }
        get
        {
            return totalExp;
        }
    }


    //データのリセット
    public void DateReset()
    {
        Level = 1;
        CurrentExp = 0;
        TotalExp = 0;
    }
}
