using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(
    menuName = "ScriptableObject/AudioManager",
    fileName = "AudioManager")
]

///音を管理するScriptableObject
///Listのラベル（element0,element1...）をenumで管理できるようにエディタ拡張しました
///参考URL
///https://goropocha.hatenablog.com/entry/2021/02/11/232617
///
public class AudioManager : ScriptableObject
{
    //BGMの名前
    public enum BGM
    {
        Title,
        Home_noon,
        Home_evening,
        Home_night,
        Formation,
        Map,
        Battle,
        BossBattle,
        Clear,
        GameOver
    }

    //サウンドエフェクトの名前
    public enum SE
    {
        TitleClick,
        Click,
        Click2,
        Back,
        SceneMove,
        SpecialButton,
        Sword1,
        Sword2,
        Fire1,
        Fire2,
        Strike,
        EnemyDie,
        Flappy1,
        Flappy2
    }

    //BGMを格納した配列
    [SerializeField,EnumIndex(typeof(BGM))]
    public AudioClip[] BGMClips;

    //SEを格納した配列
    [SerializeField, EnumIndex(typeof(SE))]
    public AudioClip[] SEClips;

    //string型で渡された名前のBGMのAudioClipを返す処理
    public AudioClip ToBGMClip(string bgm)
    {
        return BGMClips[(int)(AudioManager.BGM)Enum.Parse(typeof(AudioManager.BGM), bgm )];
    }

    //string型で渡された名前のSEのAudioClipを返す処理
    public AudioClip ToSEClip(string se)
    {
        return SEClips[(int)(AudioManager.SE)Enum.Parse(typeof(AudioManager.SE), se)];
    }
}