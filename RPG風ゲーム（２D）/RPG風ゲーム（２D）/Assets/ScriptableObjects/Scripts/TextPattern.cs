using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/TextPattern",
    fileName = "TextPattern")
]

//ホーム画面のセリフ
public class TextPattern : ScriptableObject
{
    [Header("1行10文字まで")]
    [SerializeField, TextArea(1, 4)] private string serifText;

    public string SerifText
    {
        set
        {
            serifText = value;
        }
        get
        {
            return serifText;
        }
    }
}
