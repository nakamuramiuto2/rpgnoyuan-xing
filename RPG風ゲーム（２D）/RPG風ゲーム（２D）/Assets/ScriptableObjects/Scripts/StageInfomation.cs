using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/StageInfomation",
    fileName = "StageInfomation")
]

//ステージの情報
public class StageInfomation : ScriptableObject
{
    //クリアしたステージのフラグ
    public List<bool> clearFlg = new List<bool>();

    //FirstWave　の敵
    public List<EnemySetDate> FirstEnemys = new List<EnemySetDate>();

    //SeconsWave　の敵
    public List<EnemySetDate> SecondEnemys = new List<EnemySetDate>();

    //ThirdWave　の敵
    public List<EnemySetDate> ThirdEnemys = new List<EnemySetDate>();

    //表示する敵の情報
    public List<EnemySetDate> ShowEnemys = new List<EnemySetDate>();

    //マップの番号
    public int MapNum;
    
    //ステージの番号
    public int StageNum;

    //ステージの経験値
    public int StageExp;

    //ボス戦があるかどうか
    public bool BossFlg;
}