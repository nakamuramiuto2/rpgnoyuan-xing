using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/EnemySetDate",
    fileName = "EnemySetDate")
]

//敵のデータ
public class EnemySetDate : ScriptableObject
{
    [SerializeField] private string EnemyName;  //名前
    [SerializeField] private Sprite EnemySprite;   //キャラの画像
    [SerializeField] private GameObject EnemyObj;  //キャラのオブジェクト
    [SerializeField] private int Level;   ///キャラのレベル
    [SerializeField] private float Hp;  //ヒットポイント、体力
    [SerializeField] private int Str;  //物理攻撃力
    [SerializeField] private int Def;  //物理防御力
    [SerializeField] private Skill Skill_1;　　//スキル1
    [SerializeField] private Skill Skill_2;   //スキル2
    [Header("1行10文字まで")]
    [SerializeField, TextArea(1, 10)] string EnemyIntroduction; //敵の紹介文
    [SerializeField] private int enemyExp;   // 倒すともらえる経験値

    public string GetSetName
    { 
        get 
        { 
            return EnemyName;
        } 
        set 
        { 
            EnemyName = value; 
        } 
    }
    public Sprite GetSetSprite 
    { 
        get 
        { 
            return EnemySprite; 
        } 
        set 
        { 
            EnemySprite = value; 
        } 
    }
    public GameObject GetSetObject 
    { 
        get 
        { 
            return EnemyObj; 
        } 
        set 
        { 
            EnemyObj = value;
        } 
    }
    public int GetSetLevel 
    { 
        get 
        { 
            return Level; 
        } 
        set 
        { 
            Level = value; 
        }
    }
    public float GetSetHp
    {
        get
        {
            return Hp;
        }
        set
        {
            Hp = value;
        }
    }
    public int GetSetStr
    {
        get
        {
            return Str;
        }
        set
        {
            Str = value;
        }
    }
    public int GetSetDef
    {
        get
        {
            return Def;
        }
        set
        {
            Def = value;
        }
    }
    public Skill GetSetSkill_1
    {
        get
        {
            return Skill_1;
        }
        set
        {
            Skill_1 = value;
        }
    }
    public Skill GetSetSkill_2
    {
        get
        {
            return Skill_2;
        }
        set
        {
            Skill_2 = value;
        }
    }

    public string GetSetIntroduction
    {
        get
        {
            return EnemyIntroduction;
        }
        set
        {
            EnemyIntroduction = value;
        }
    }

    public int Exp
    {
        set
        {
            enemyExp = value;
        }
        get
        {
            return enemyExp;
        }
    }
}
