using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/CharacterSetDate",
    fileName = "CharacterSetDate")
]

//キャラクターのデータ
public class CharacterSetDate : ScriptableObject
{
    public enum CHARACTER_POSITION
    {
        前衛,
        中衛,
        後衛,
    }
    [SerializeField] private string CharacterName;  //名前
    [SerializeField] private Sprite CharaSprite;   //キャラの画像
    [SerializeField] private GameObject CharaObj;  //キャラのオブジェクト
    [Header("Animationのみ行うPrefab")]
    public GameObject CharaObj_anim;
    [SerializeField] private int Level;   ///キャラのレベル
    [SerializeField] private float HP;　　//ヒットポイント、体力
    [SerializeField] private int STR;　　//物理攻撃力
    [SerializeField] private int INT;　　 //魔法攻撃力
    [SerializeField] private int DEF;  //物理防御力
    [SerializeField] private int RES;  //魔法防御力
    [SerializeField] private int AGI;  //すばやさ
    public CHARACTER_POSITION Character_Position;
    [SerializeField] private Skill Skill_1;
    [SerializeField] private Skill SpecialSkill;


    public string GetSetName { get { return CharacterName; } set { CharacterName = value; } }
    public Sprite GetSetCharaSprite { get { return CharaSprite; } set { CharaSprite = value; } }
    public GameObject  GetSetCharaObj { get { return CharaObj; } set { CharaObj = value; } }
    public int GetSetLevel { get { return Level; } set { Level = value; } }
    public float GetSetHP { get { return HP; } set { HP = value; } }
    public int GetSetSTR { get { return STR; } set { STR = value; } }
    public int GetSetINT { get { return INT; } set { INT = value; } }
    public int GetSetDEF { get { return DEF; } set { DEF = value; } }
    public int GetSetRES { get { return RES; } set { RES = value; } }
    public int GetSetAGI { get { return AGI; } set { AGI = value; } }
    public CHARACTER_POSITION GetSetCharacter_Positin { get { return Character_Position; } set { Character_Position = value; } }
    public Skill GetSetSkill { get { return Skill_1; } set { Skill_1 = value; } }
    public Skill GetSetSpecialSkill { get { return SpecialSkill; } set { SpecialSkill = value; } }

}