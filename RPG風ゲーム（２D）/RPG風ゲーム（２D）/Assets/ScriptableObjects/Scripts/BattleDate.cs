using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/BattleDate",
    fileName = "BattleDate")
]

//バトルの進行に関するデータ
public class BattleDate : ScriptableObject
{

    /*バトルを管理するフラグ
     * true　⇒　バトル　開始
     * false　⇒　バトル　終了、中断
     */
    [SerializeField] private bool battleFlg = false;

    //現在編成されているキャラクター
    public CharacterSetDate[] CurrentCharacter = new CharacterSetDate[5];

    public bool GetSetbuttleFlg { get { return battleFlg; } set { battleFlg = value; } }

}

