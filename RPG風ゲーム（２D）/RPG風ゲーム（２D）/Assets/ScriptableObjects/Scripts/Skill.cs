using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/Skill",
    fileName = "Skill")
]

//�G�A�L�����N�^�[���ʂ̃X�L���̃f�[�^
public class Skill : ScriptableObject
{
    [SerializeField] private string SkillName;   //�Z��

    public enum SKILLTYPE
    {
        Recovery,   //�񕜃X�L��
        SingleAttack,   //�P�̍U��
        MultipleAttack,   //�͈͍U��
    }
    public SKILLTYPE skillType;
    [Header("�_���[�W�͂P�`1000")]
    [SerializeField] private int SkillDamage;   //�Z�̊�b�_���[�W
    [Range(0,4)]
    [SerializeField] private int MaxPosition;   //�Z�̗L���͈�
    [SerializeField] private float CoolTime;   // �Z�̃N�[���^�C��
    [Header("�����˕K�E���A�G��2��ڂ̋Z��")]
    [SerializeField] private bool SpecialSkillFlg = false;  //�Z���K�E�Z���ǂ���

    [SerializeField] private Sprite skillSprite;  //�X�L���̉摜

    public SKILLTYPE GetSetSKILLTYPE { get { return skillType; } set { skillType = value; } }
    public string GetSetSkillName { get { return SkillName; } set { SkillName = value; } }
    public int GetSetSkillDamage { get { return SkillDamage; } set { SkillDamage = value; } }
    public int GetSetMaxPosition { get { return MaxPosition; } set { MaxPosition = value; } }
    public float GetSetCoolTime { get { return CoolTime; } set { CoolTime = value; } }
    public bool GetSetSpecialSkillFlg { get { return SpecialSkillFlg; } set { SpecialSkillFlg = value; } }
    
    public Sprite GetSetSkillSprite { set { skillSprite = value; } get { return skillSprite; } }

}