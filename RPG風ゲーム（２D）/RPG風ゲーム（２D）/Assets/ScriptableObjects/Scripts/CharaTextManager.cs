using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    menuName = "ScriptableObject/CharaTextManager",
    fileName = "CharaTextManager")
]

public class CharaTextManager : ScriptableObject
{
    [SerializeField] private string characterName;  //対応するキャラクターの名前
    [Header("いつでも言うセリフ")]
    [SerializeField] private List<TextPattern> characterTexts = new List<TextPattern>();  //キャラクターのセリフ（いつでも）
    [Header("昼だけ言うセリフ")]
    [SerializeField] private TextPattern noonText; //キャラクターのセリフ（昼だけ）
    [Header("夕方だけ言うセリフ")]
    [SerializeField] private TextPattern evenigText;  //キャラクターのセリフ（夕方だけ）
    [Header("夜だけ言うセリフ")]
    [SerializeField] private TextPattern nightText;  //キャラクターのセリフ（夜だけ）

    public string Name
    {
        set
        {
            characterName = value;
        }
        get
        {
            return characterName;
        }
    }

    int tmpNum;  //1つ前のテキストと被らないため　1つ前のテキスト番号を格納(最初は１万)


    /*TextPattern を返す処理
     * firstFlg = true  →　最初
     * 　　　　 = false →　2回目以降
     * backImage = 0  →　昼
     * 　　　　　= 1  →　夕方
     * 　　　　　= 2  →　夜
     */
    public TextPattern SendText(bool firstFlg,int backImage)
    {
        if(firstFlg)
        {
            switch (backImage)
            {
                case 0:
                    characterTexts.Add(noonText);
                    break;
                case 1:
                    characterTexts.Add(evenigText);
                    break;
                case 2:
                    characterTexts.Add(nightText);
                    break;
            }

            tmpNum = Random.Range(0, characterTexts.Count);
            Debug.Log(tmpNum);
            return characterTexts[tmpNum];
        }
        else 
        {
            int textNum;
            do
            {
                textNum = Random.Range(0, characterTexts.Count);
            }
            while (textNum == tmpNum);

            tmpNum = textNum;
            return characterTexts[tmpNum];
        }
    }

    //追加したテキスト（昼、夕方、夜）があれば削除
    public void RemoveList()
    {
        if(characterTexts.Contains(noonText) || characterTexts.Contains(evenigText) || characterTexts.Contains(nightText))
        characterTexts.RemoveAt(characterTexts.Count - 1);
    }
}