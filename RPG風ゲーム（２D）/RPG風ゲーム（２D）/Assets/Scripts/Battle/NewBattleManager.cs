using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

//現在のWave
public enum WAVE
{
    First,
    Second,
    Third,
    Clear,
    GameOver
}

//バトルを管理する処理
public class NewBattleManager : MonoBehaviour
{
    public WAVE wave;

    //必要なデータ
    public BattleDate battleDate;
    public CharacterDeploy charaDeploy;
    public CameraMove mainCamera;
    public GameObject startWindow;
    public Text windowText;
    public GameObject gameOverImage;
    public AudioController audioController;

    /*キャラクターのデータ
     * SortedList <キャラの位置,キャラのデータ>
     */
    public SortedList<int, CharaStateChange> CharaList = new SortedList<int, CharaStateChange>();

    /*敵のデータ
     * SortedList <敵の位置,敵のデータ>
     */
    public SortedList<int, EnemyStateChange> EnemyList = new SortedList<int, EnemyStateChange>();

    void Start()
    {
        wave = WAVE.First;
        startWindow.SetActive(false);
        gameOverImage.SetActive(false);
        battleDate.GetSetbuttleFlg = false;

        StartCoroutine(BattleStart());
    }

    void Update()
    {

    }

    //バトル開始の処理　バトルの進行はBattleDate の　battleFlg が管理
    private IEnumerator BattleStart()  //バトル開始
    {
        yield return new WaitForSeconds(1.0f);
        battleDate.GetSetbuttleFlg = true;
        windowText.text = "Start";
        startWindow.SetActive(true);
        StartCoroutine(PutOutUI());
    }

    //UIを非表示
    private IEnumerator PutOutUI()
    {
        yield return new WaitForSeconds(2.0f);
        startWindow.SetActive(false);
    }

    //キャラクターを追加
    public void InCharacterList(int Point,CharaStateChange Character)
    {
        CharaList.Add(Point, Character);
    }

    //敵を追加
    public void InEnemyList(int Point,EnemyStateChange Enemy)
    {
        EnemyList.Add(Point, Enemy);
    }

    /*キャラクターを削除
     * キャラの数が0になるとゲームオーバー
     */
    public void RemoveCharaList(int Position)
    {
        CharaList.Remove(Position);

        if(CharaList.Count <= 0)
        {
            wave = WAVE.GameOver;
            StartCoroutine(NextWave());
        }
    }

    /*敵を削除
     * 敵の数が0になると
     * Wave移行　または　ステージクリア
     */
    public void RemoveEnemyList(int Position)
    {
        EnemyList.Remove(Position);

        if(EnemyList.Count <= 0)
        {

            if (wave == WAVE.First)
            {
                wave = WAVE.Second;
            }
            else if(wave == WAVE.Second)
            {
                wave = WAVE.Third;
            }
            else if(wave == WAVE.Third)
            {
                wave = WAVE.Clear;
            }

            switch(wave)
            {
                case WAVE.Second:
                    charaDeploy.NextWave(WAVE.Second);
                    break;
                case WAVE.Third:
                    charaDeploy.NextWave(WAVE.Third);
                    break;
                case WAVE.Clear:
                    Debug.Log("Stage Clear");
                    break;
                default:
                    break;
            }
            StartCoroutine(NextWave());
        }
    }

    /*キャラクター　⇒　敵への　単体攻撃
     * 敵の先頭にダメージを与える
     */
    public void DamageToEnemy_Single(int Damage)
    {
        KeyValuePair<int, EnemyStateChange> pair = EnemyList.First();
        pair.Value.Damaged(Damage);
    }


    /*キャラクター　⇒　敵への　範囲攻撃
     * 攻撃の届く範囲内にいる敵にダメージを与える
     */
    public void DamageToEnemy_Multipul(int MaxPosition, int Damage)
    {
        foreach (KeyValuePair<int, EnemyStateChange> pair in EnemyList)
        {
            if (pair.Key <= MaxPosition)
            {
                pair.Value.Damaged(Damage);
            }
        }
    }
    /*敵　⇒　キャラクターへの　単体攻撃
     * キャラクターの先頭にダメージを与える
     * 
     */
    public void DamageToCharacter_Single(int Damage)
    {
        KeyValuePair<int, CharaStateChange> pair = CharaList.First();
        pair.Value.Damaged(Damage);
    }


    /*敵　⇒キャラクター　への　範囲攻撃
     * キャラクターの先頭にダメージを与える
     */
    public void DamageToCharacter_Multipul(int MaxPosition, int Damage)
    {
        foreach (KeyValuePair<int, CharaStateChange> pair in CharaList)
        {
            if (pair.Key <= MaxPosition)
            {
                pair.Value.Damaged(Damage);
            }
        }
    }

    //次のWaveへ移行
    IEnumerator NextWave()
    {
        battleDate.GetSetbuttleFlg = false;

        yield return new WaitForSeconds(1.0f);


        //クリアシーンへ
        if(wave == WAVE.Clear)
        {
            foreach (KeyValuePair<int, CharaStateChange> pair in CharaList)
            {
                pair.Value.Idle();
            }

            windowText.text = "StageClear";
            startWindow.SetActive(true);
            yield return new WaitForSeconds(1.5f);
            SceneManager.LoadScene("BattleClear");
        }
        //ゲームオーバー
        else if(wave == WAVE.GameOver)
        {
            audioController.PlayBGMSound("GameOver");
            gameOverImage.SetActive(true);

            foreach (KeyValuePair<int, EnemyStateChange> pair in EnemyList)
            {
                pair.Value.Idle();
            }
        }
        //次のWaveへ
        else
        {
            foreach (KeyValuePair<int, CharaStateChange> pair in CharaList)
            {
                pair.Value.Run();
                mainCamera.MainCameraMove();
            }
        }
    }

}
