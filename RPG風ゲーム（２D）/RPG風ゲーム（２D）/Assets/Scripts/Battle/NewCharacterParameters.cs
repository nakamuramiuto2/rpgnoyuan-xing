using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//キャラクターのパラメータ
public class NewCharacterParameters : MonoBehaviour
{
    [SerializeField] private float copyHP;
    [SerializeField] private int copySTR;
    [SerializeField] private int copyINT;
    [SerializeField] private int copyDEF;
    [SerializeField] private int copyRES;
    [SerializeField] private int copyAGI;
    [SerializeField] private int copyPosition;
    [SerializeField] private Skill nomalSkill;
    [SerializeField] private Skill specialSkill;
    public BattleDate battleDate;

    public float HP
    {
        get
        {
            return copyHP;
        }

        set
        {
            copyHP = value;
        }
    }
    public int STR
    {
        get
        {
            return copySTR;
        }

        set
        {
            copySTR = value;
        }
    }
    public int INT
    {
        get
        {
            return copyINT;
        }

        set
        {
            copyINT = value;
        }
    }
    public int DEF
    {
        get
        {
            return copyDEF;
        }

        set
        {
            copyDEF = value;
        }
    }
    public int RES
    {
        get
        {
            return copyRES;
        }

        set
        {
            copyRES = value;
        }
    }
    public int AGI
    {
        get
        {
            return copyAGI;
        }

        set
        {
            copyAGI = value;
        }
    }
    public int Position
    {
        get
        {
            return copyPosition;
        }

        set
        {
            copyPosition = value;
        }
    }
    public Skill NomalSkill
    {
        get
        {
            return nomalSkill;
        }

        set
        {
            nomalSkill = value;
        }
    }
    public Skill SpecialSkill
    {
        get
        {
            return specialSkill;
        }

        set
        {
            specialSkill = value;
        }
    }

    //キャラクターのパラメータをセット
    public void SetChara(int buttonNum)
    {
        HP = battleDate.CurrentCharacter[buttonNum].GetSetHP;
        STR = battleDate.CurrentCharacter[buttonNum].GetSetSTR;
        INT = battleDate.CurrentCharacter[buttonNum].GetSetINT;
        DEF = battleDate.CurrentCharacter[buttonNum].GetSetDEF;
        RES = battleDate.CurrentCharacter[buttonNum].GetSetRES;
        AGI = battleDate.CurrentCharacter[buttonNum].GetSetAGI;
        Position = buttonNum;
        NomalSkill = battleDate.CurrentCharacter[buttonNum].GetSetSkill;
        SpecialSkill = battleDate.CurrentCharacter[buttonNum].GetSetSpecialSkill;
    }
}
