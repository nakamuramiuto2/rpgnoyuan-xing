using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

//バトルシーンでのキャラ、敵生成
public class CharacterDeploy : MonoBehaviour
{
    //Boss戦のときにBGMを変える関数を入れる
    [Serializable] private class SceneMoveEvents : UnityEvent { }
    [SerializeField] private SceneMoveEvents myEvent = null;

    //キャラを生成する場所
    public List<GameObject> CharaSpawnPoint = new List<GameObject>();

    //それぞれ敵を生成する場所
    public List<GameObject> EnemyFirstPosition = new List<GameObject>();
    public List<GameObject> EnemySecondPosition = new List<GameObject>();
    public List<GameObject> EnemyThirdPosition = new List<GameObject>();

    public BattleDate battleDate;
    public StageInfomation stageInfo;

    public Text waveText;

    // Start is called before the first frame update
    void Start()
    {
        waveText.text = "WAVE 1";

        for(int i = 0; i < 5;i++)
        {
            //キャラクターの生成
            GameObject obj = Instantiate(battleDate.CurrentCharacter[i].GetSetCharaObj, CharaSpawnPoint[i].transform.position, 
                battleDate.CurrentCharacter[i].GetSetCharaObj.transform.rotation);
            //キャラクターのボタンの番号を設定
            obj.GetComponent<CharaRegistration>().ButtonNum = i;

            //生成した順にタグを変える
            switch (i)
            {
                case 0: case 1:
                    obj.tag = "Character_forward";
                    break;
                case 2: case 3:
                    obj.tag = "Character_middle";
                    break;
                case 4:
                    obj.tag = "Character_back";
                    break;
                default:
                    break;

            }

        }
        //敵の生成（Wave 1）
        for(int i = 0;i < stageInfo.FirstEnemys.Count;i++)
        {
            GameObject obj = Instantiate(stageInfo.FirstEnemys[i].GetSetObject, EnemyFirstPosition[i].transform.position,
                stageInfo.FirstEnemys[i].GetSetObject.transform.rotation);
            EnemyRegistration enemy = obj.GetComponent<EnemyRegistration>();
            enemy.positionNum = i;
            enemy.enemyDate = stageInfo.FirstEnemys[i];
        }
    }

    /*Waveの変更
     * 引数　⇒　次のWave
     */
    public void NextWave(WAVE wave)
    {
        switch(wave)
        {
            case WAVE.Second:  //Wave 2 の敵を生成

                waveText.text = "WAVE 2";

                for (int i = 0; i < stageInfo.SecondEnemys.Count; i++)
                {
                    GameObject obj = Instantiate(stageInfo.SecondEnemys[i].GetSetObject, EnemySecondPosition[i].transform.position, 
                        stageInfo.SecondEnemys[i].GetSetObject.transform.rotation);
                    EnemyRegistration enemy = obj.GetComponent<EnemyRegistration>();
                    enemy.positionNum = i;
                    enemy.enemyDate = stageInfo.SecondEnemys[i];
                }
                break;

            case WAVE.Third:  //Wave 3　の敵を生成

                waveText.text = "WAVE 3";

                for (int i = 0; i < stageInfo.ThirdEnemys.Count; i++)
                {
                    GameObject obj = Instantiate(stageInfo.ThirdEnemys[i].GetSetObject, EnemyThirdPosition[i].transform.position,
                        stageInfo.ThirdEnemys[i].GetSetObject.transform.rotation);
                    EnemyRegistration enemy = obj.GetComponent<EnemyRegistration>();
                    enemy.positionNum = i;
                    enemy.enemyDate = stageInfo.ThirdEnemys[i];

                }

                //Boss戦だとBGM変更
                if(stageInfo.BossFlg)
                {
                    myEvent.Invoke();
                }
                break;
            default:
                break;
        }
    }
}
