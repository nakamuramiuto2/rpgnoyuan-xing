using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//バトルシーンでのキャラとカメラを止める処理
public class Chara_Camera_Stop : MonoBehaviour
{
    //止める場所の種類
    public enum Position
    {
        Forward,
        Middle,
        Back,
        Camera
    }

    //キャラタグの名前
    string tagName;
    public Position _position;
    public BattleDate battleDate;

    // Start is called before the first frame update
    void Start()
    {
        /*止める場所のPosition（enum）によって
         * 止めるキャラクターを決める
         * （タグで判別）
         */
        switch (_position)
        {
            case Position.Forward:
                tagName = "Character_forward";
                break;
            case Position.Middle:
                tagName = "Character_middle";
                break;
            case Position.Back:
                tagName = "Character_back";
                break;
            case Position.Camera:
                tagName = "MainCamera";
                break;
            default:
                break;
        }
    }

    //指定したタグのキャラ、カメラを止める
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.CompareTag(tagName))
        {
            if(_position != Position.Camera)
            {
                coll.gameObject.GetComponent<CharaStateChange>().Idle();
            }
            else
            {
                coll.gameObject.GetComponent<CameraMove>().MainCameraStop();
                StartCoroutine(WaveStart());
            }
        }
    }

    //中断しているWave（戦いを再開させる）
    private IEnumerator WaveStart()
    {
        yield return new WaitForSeconds(0.5f);
        battleDate.GetSetbuttleFlg = true;
    }
}
