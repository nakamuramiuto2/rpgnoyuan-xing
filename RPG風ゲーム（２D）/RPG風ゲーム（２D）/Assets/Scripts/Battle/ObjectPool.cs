using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//ダメージテキストをオブジェクトプールとして扱う処理
public class ObjectPool : MonoBehaviour
{
     private static ObjectPool _instance;

    public static ObjectPool instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ObjectPool>();
            }

            return _instance;
        }
    }
    //ゲームオブジェクトのDictionary
    private Dictionary<string, Queue<GameObject>> pooledGameObjects = new Dictionary<string, Queue<GameObject>>();

    //ダメージテキストを取得する　必要ならば生成する
    public GameObject GetGameObject(GameObject prefab, Vector2 position, Quaternion rotation,int damage)
    {
        //プレハブのIDをkeyとする
        string key = prefab.name;

        //keyがなければ生成
        if(pooledGameObjects.ContainsKey(key) == false)
        {
            pooledGameObjects.Add(key, new Queue<GameObject>());
        }

        Queue<GameObject> gameObjects = pooledGameObjects[key];

        GameObject Obj = null;

        //オブジェクトの再利用
        if (gameObjects.Count > 0)
        {
            Obj = gameObjects.Dequeue();
            TextMesh damageText = Obj.GetComponent<TextMesh>();
            damageText.text = "" + damage;
            Obj.transform.position = position;
            Obj.transform.rotation = rotation;
            Obj.SetActive(true);
        }
        //オブジェクトの生成
        else
        {
            Obj = (GameObject)Instantiate(prefab, position, rotation);
            Obj.name = prefab.name;

            TextMesh damageText = Obj.GetComponent<TextMesh>();
            damageText.text = "" + damage;

            Obj.transform.parent = gameObject.transform;
        }

        return Obj;
    }

    //オブジェクトを非アクティブにする
    public void ReleaseGameObject(GameObject Obj)
    {
        Obj.SetActive(false);
        string key = Obj.name;
        Queue<GameObject> gameObjects = pooledGameObjects[key];
        gameObjects.Enqueue(Obj);
    }
}
