using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//バトルシーンのメニュー画面の処理
public class MenuManager : MonoBehaviour
{
    public BattleDate battleDate;

    //表示するメニュー画面
    public GameObject menuWindow;
    public Button menuButton;
    public Button questionButton;
    public bool menuFlg = false;

    // Start is called before the first frame update
    IEnumerator Start()
    { 
        menuWindow.SetActive(false);

        ///シーン移動後すぐにメニューボタンを押すと
        ///メニューウィンドウが出たままバトルが進行する
        ///→止めるための処理
        ///(遊び方ボタンも同様)
        menuButton.interactable = false;
        questionButton.interactable = false;
        yield return new WaitForSeconds(1.0f);
        menuButton.interactable = true;
        questionButton.interactable = true;
    }

    //メニューボタンを押した際の処理
    public void OnClick_OpenCloseMenu()
    {
        if(menuFlg == false)
        {
            menuWindow.SetActive(true);
            menuButton.interactable = false;
            battleDate.GetSetbuttleFlg = false;
        }
        else
        {
            menuWindow.SetActive(false);
            menuButton.interactable = true;
            battleDate.GetSetbuttleFlg = true;
        }

        menuFlg = !menuFlg;
    }

    /*シーン移動　バトルシーンのリスタート
     * 引数　⇒　移動するシーン先の名前
     */
    public void TransitionScene(string SceneName)
    {
        if(SceneName == "BatttleScene")
        {
            Scene loadScene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(loadScene.name);
            return;
        }

        SceneManager.LoadScene(SceneName);
    }
}
