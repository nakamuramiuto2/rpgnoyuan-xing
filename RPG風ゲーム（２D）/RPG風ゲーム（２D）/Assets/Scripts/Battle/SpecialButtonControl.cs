using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//バトルシーンに移行した際にスペシャルボタンの画像をキャラの画像にする
public class SpecialButtonControl : MonoBehaviour
{
    public SkillCounter[] SpecialButtons = new SkillCounter[5];
    public Button[] CharaButtons = new Button[5];
    public BattleDate battleDate;

    void Start()
    {
        for(int i = 0;i < 5;i++)
        {
            Sprite CharaSprite = battleDate.CurrentCharacter[i].GetSetCharaSprite;
            CharaButtons[i].GetComponent<Image>().sprite = CharaSprite;
        }
    }
}
