using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//カメラの移動
public  class CameraMove : MonoBehaviour
{
    Rigidbody2D rb;
    public float speed = 10.0f;

    //カメラの移動
    public void MainCameraMove()
    {
        if(rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
        }

        rb.velocity = new Vector2(speed, 0);
    }

    //指定の場所でストップ
    public void MainCameraStop()
    {
        rb.velocity = Vector2.zero;
    }
}
