using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*Battleシーンにあるキャラクターボタンの処理
 */
public class NewCharaButtonWorking : MonoBehaviour
{
    //　↓　必要な情報
    public CharaStateChange stateChange;
    public NewCharacterParameters character;
    public BattleDate battleDate;
    public NewBattleManager battleManager;
    public AutButtonChange A_B_Change;
    protected int ButtonNum;

    //　↓　HP、Skillゲージ、Specialゲージ
    public Slider HpSlider;
    public Button SkillButton;
    public Button SpecialButton;
    public Image SkillGauge;
    public Slider SpecialGauge;

    //　↓　スキルのカウント
    protected float SkillCount;
    protected float SkillMeter;
    public Text CountText;
    protected float SpecialCount = 0.0f;

    //各キャラ毎に設定されたフルオートボタンのフラグ
    public bool AutFlg = false;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        HpSlider.maxValue = character.HP;
        SkillCount = character.NomalSkill.GetSetCoolTime;
        SpecialGauge.maxValue = character.SpecialSkill.GetSetCoolTime;
        ButtonOnOff("Both", false);
        yield break;
    }

    public void Update()
    {
        HpSlider.value = character.HP;

        //バトルが進行している　且つ　キャラが生きている　時にスキルゲージがたまる
        if (battleDate.GetSetbuttleFlg)
        {
            if (character.HP >= 0)
            {
                CountMeter();

                if (SkillCount <= 0)
                {
                    ButtonOnOff("SkillButton", true);

                }
                if (SpecialGauge.value >= character.SpecialSkill.GetSetCoolTime)
                {
                    ButtonOnOff("SpecialButton", true);
                }
            }
        }
        else
        {
            ButtonOnOff("Both", false);
        }
    }

    //SkillCounter　Script　にて処理
    public virtual void CountMeter()
    {

    }

    //フルオートボタンを押した時の処理
    public void OnClick_AutmaticButton()
    {
        if(!AutFlg)
        {
            AutFlg = true;
            A_B_Change.Change_AfterImage();
        }
        else
        {
            Debug.Log("AutFlg = false");
            AutFlg = false;
            A_B_Change.Change_BeforeImage();
        }
    }

    /*ボタンを押せなくする　押せるようにする　処理
     * 引数　⇒　ButtonNum - 処理をしたいボタンの名前
     * 　　　　　　　　　　　Both の場合両方
     * 　　　　　OnFlg　-　true ⇒ 押せるようにする
     * 　　　　　          false ⇒ 押せなくする
     */
    public void ButtonOnOff(string ButtonName,bool OnFlg)
    {
        switch (ButtonName)
        {
            case "SkillButton":
                SkillButton.interactable = OnFlg;
                break;
            case "SpecialButton":
                SpecialButton.interactable = OnFlg;
                break;
            case "Both":
                SkillButton.interactable = OnFlg;
                SpecialButton.interactable = OnFlg;
                break;
            default:
                break;  
        }
    }
}
