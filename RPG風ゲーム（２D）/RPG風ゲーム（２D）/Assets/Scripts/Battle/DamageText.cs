using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*ダメージテキストの処理
 * ダメージテキストはオブジェクトプールとして処理される
 */
public class DamageText : MonoBehaviour
{
    //消える時間
    [SerializeField] private float DeleteTime = 1.0f;

    //最初のAlpha値
    [SerializeField] private float alpha = 1.0f;
    //最後のAlpha値
    [SerializeField] private float EndAlpha = 0.0f;

    //動くスピード
    [SerializeField] private float MoveSpeed = 0.1f;

    private float TimeCount;
    private TextMesh NowText;

    // Start is called before the first frame update
    void Start()
    {
        TimeCount = 0.0f;
        NowText = gameObject.GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        OnEnableText();
    }

    //表示された際に実行
    private void OnEnableText()
    {
        TimeCount += Time.deltaTime;
        alpha = 1.0f - (1.0f - EndAlpha) * (TimeCount / DeleteTime);
        transform.position += Vector3.up * MoveSpeed;

        if (alpha <= 0.1f)
        {
            ObjectPool.instance.ReleaseGameObject(gameObject);
        }

        NowText.color = new Color(NowText.color.r, NowText.color.g, NowText.color.b, alpha);
    }

    //非表示になった際に実行
    private void OnDisable()
    {
        TimeCount = 0.0f;
        alpha = 1.0f;
        NowText.color = new Color(NowText.color.r, NowText.color.g, NowText.color.b, alpha);
    }
}
