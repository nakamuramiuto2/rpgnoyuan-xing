using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ダメージ計算
public static class DamageCalculation
{
    /*　計算式　基礎攻撃力　=　キャラクターの攻撃力　+ スキルダメージ
     * 　　　　　
     *　　　　　受けるダメージ　=　基礎攻撃力　/　防御力
     * 
     */
    public static int BasicsPower(int CharaSTR,int SkillPower)
    {
        int Damage;
        Damage = CharaSTR + SkillPower;
        return Damage;
    }

    public static int DamageCalcu(int BasicsDamage,int CharaDEF)
    {
        int Damage;
        Damage = BasicsDamage / CharaDEF;

        if(Damage <= 0)
        {
            Damage = 1;
        }
        return Damage;
    }
}
