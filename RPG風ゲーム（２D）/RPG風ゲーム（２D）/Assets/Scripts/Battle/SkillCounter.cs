using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//キャラクターのスキル、スペシャルボタンの処理
public class SkillCounter : NewCharaButtonWorking
{
    /*スキルの発動時間の計測
     * 表示
     */
    public override void CountMeter()
    {
        SkillCount -= Time.deltaTime;
        SkillMeter = SkillCount / character.NomalSkill.GetSetCoolTime;
        CountText.text = SkillCount.ToString("F1");
        SkillGauge.fillAmount = SkillMeter;

        if(SkillCount <= 0)
        {
            SkillCount = 0.0f;
            SkillMeter = 1.0f;

            if(AutFlg && character.HP >= 0)
            {
                OnClickSkillButton();
            }
        }
    }

    /*スキルボタンを押した際の処理
     * クールタイムのリセット
     */
    public void OnClickSkillButton()
    {
        stateChange.Attack();
        SkillCount = character.NomalSkill.GetSetCoolTime;
        SkillMeter = 0.0f;
        ButtonOnOff("SkillButton", false);
        ChargeSpecialSkill();
    }

    /* スキルを撃った時にスペシャルゲージが溜まる
     * スペシャルゲージが　スペシャルスキルの
     * クールタイム　×　1.5　になったら
     * 必殺技が放てる 
     */
    public void ChargeSpecialSkill()
    {
        SpecialCount += character.NomalSkill.GetSetCoolTime * 1.5f;
        SpecialGauge.value += SpecialCount;
        SpecialCount = 0.0f;

        if(SpecialGauge.value >= character.SpecialSkill.GetSetCoolTime)
        {
            SpecialGauge.value = SpecialGauge.maxValue;

            if(AutFlg)
            {
                OnClickSpecialButton();
            }

        }
    }

    /*スペシャルボタンを押した時の処理
     * スペシャルゲージのリセット
     */

    public void OnClickSpecialButton()
    {
        stateChange.SpecialAttack();
        SpecialGauge.value = 0.0f;
        ButtonOnOff("SpecialButton", false);
    }
}
