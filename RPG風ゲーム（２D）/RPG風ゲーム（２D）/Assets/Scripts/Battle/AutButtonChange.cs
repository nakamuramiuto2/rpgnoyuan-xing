using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//キャラクターのフルオートボタンの画像変更処理
public class AutButtonChange : MonoBehaviour
{
    public Sprite beforeImage;
    public Sprite afterImage;
    public Image currentImage;

    public void Change_AfterImage()
    {
        currentImage.sprite = afterImage;
    }

    public void Change_BeforeImage()
    {
        currentImage.sprite = beforeImage;
    }
}
