using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


/*　バトルクリアした後の、クリアシーンの処理
 * 1.キャラクターの経験値　加算
 * 2.テキスト表示
 * 3.キャラクターの生成
 */

public class BattleResult : MonoBehaviour
{
    //必要な情報
    public StageInfomation stageInfo;
    public BattleDate battleDate;

    //表示する情報
    public List<Text> nameTexts = new List<Text>();
    public List<Text> levelText = new List<Text>();
    public List<GameObject> spawnPoint = new List<GameObject>();
    public Text ClearText;
    string clearTex;

    //手に入れる経験値
    public int totalExp;
    //仮の変数
    public int tmpExp;
    public float timeCount;

    //Playerのデータ
    public PlayerDate playerDate;
    public Text expText;
    public Slider expSlider;

    bool levelUpFlg = true;

    private void Start()
    {
        //ステージ情報の表示
        clearTex = stageInfo.MapNum + "-" + stageInfo.StageNum;
        ClearText.text = clearTex + "Clear!";

        ShowCharaResult();
        SpawnCharacter();

        //ステージ解放
        if(stageInfo.StageNum != stageInfo.clearFlg.Count)
        {
            stageInfo.clearFlg[stageInfo.StageNum] = true;
        }

        SetExp();

    }

    private void Update()
    {
        //キャラレベル表示
        expText.text = "Level:" + playerDate.Level;
        expSlider.value = playerDate.CurrentExp;

        if (levelUpFlg)
        {
            AddExp();
        }
    }


    //キャラクターのリザルトの計算と表示
    private void ShowCharaResult()
    {
        int CurrentLevel;

        for(int i = 0;i < 5;i++)
        {
            CurrentLevel = battleDate.CurrentCharacter[i].GetSetLevel;

            nameTexts[i].text = battleDate.CurrentCharacter[i].GetSetName;
            levelText[i].text = battleDate.CurrentCharacter[i].GetSetLevel + "⇒" + CurrentLevel;
        }
    }

    //キャラクターの生成
    private void SpawnCharacter()
    {
        for(int i = 0;i < 5;i++)
        {
            GameObject obj = Instantiate(battleDate.CurrentCharacter[i].CharaObj_anim, spawnPoint[i].transform.position, Quaternion.identity);
        }
    }

    //プレイヤーの経験値の値のセットとBossFlgをリセット
    public void SetExp()
    {
        expSlider.maxValue = playerDate.levelExp[playerDate.Level + 1];
        expSlider.value = playerDate.CurrentExp;
        totalExp = stageInfo.StageExp;
        playerDate.TotalExp += totalExp;
        tmpExp = 0;
        stageInfo.BossFlg = false;
    }

    /*プレイヤーの経験値の計算と加算
     * 0.05秒に1経験値加算する
     */
    public void AddExp()
    {
        if (playerDate.Level == 10)
            return;

        timeCount += Time.deltaTime;

        if (timeCount >= 0.05)
        {
            playerDate.CurrentExp++;
            tmpExp++;

            if (playerDate.CurrentExp == expSlider.maxValue)
            {
                playerDate.Level++;
                playerDate.CurrentExp = 0;
                expSlider.maxValue = playerDate.levelExp[playerDate.Level + 1];
                expSlider.value = 0;
            }

            timeCount = 0;
        }

        if (totalExp == tmpExp)
        {
            levelUpFlg = false;
        }

    }
}
