using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//アニメーションのみを行うキャラクターの処理

public class CharaAnimation : MonoBehaviour
{
    //Animetionの種類
    public enum AnimType
    {
        Idle,
        Attack,
        Hurt,
        Special,
        Run,
    }

    Animator anim;
    public AnimType animType = AnimType.Idle;
    [SerializeField] private List<int> randomList = new List<int> {};　//ランダムにアニメーションの変更　⇒　パターンをListで管理
    int randomNum;
    public Button animButton;

    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetTrigger("idle");
        animButton.interactable = true;
    }

    void Update()
    {
        switch(animType)
        {
            case AnimType.Idle:
                anim.SetTrigger("idle");
                break;
            case AnimType.Attack:
                anim.SetTrigger("attack");
                    break;
            case AnimType.Hurt:
                anim.SetTrigger("hurt");
                break;
            case AnimType.Special:
                anim.SetTrigger("special");
                break;
            case AnimType.Run:
                anim.SetBool("run", true);
                break;
            default:
                break;
        }
    }
    
    //キャラクターをクリックした際にアニメーション変更
    public void OnClick()
    {
        animButton.interactable = false;
        randomNum = RamdomValue();
        animType = (AnimType)randomNum;

        if (animType != AnimType.Idle)
        {
            StartCoroutine(ReturnIdle());
        }
    }

    //Idle状態に戻る処理
    IEnumerator ReturnIdle()
    {
        yield return new WaitForSeconds(1.0f);
        anim.SetBool("run", false);
        animType = AnimType.Idle;
        animButton.interactable = true;
        yield break;
    }


    /*アニメーションをランダムに変更
     * Listにアニメーションの番号を入れ、抽出
     */
    public int RamdomValue()
    {
        if(randomList.Count <= 1)
        {
            for(int i = 0;i < 5;i++)
            {
                int randomValue = Random.Range(1, 5);
                randomList.Add(randomValue);
            }
        }
        randomList.RemoveAt(0);
        return randomList[0];
    }

}
