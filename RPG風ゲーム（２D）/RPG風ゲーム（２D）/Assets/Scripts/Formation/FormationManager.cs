using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*Formationシーン（キャラの詳細）での処理
 */
public class FormationManager : MonoBehaviour
{
    public GameObject spawnPoint;
    public CharacterSetDate charaDate;
    public GameObject characterWindow;　　　//キャラクター詳細
    public GameObject skillWindow;          //スキル詳細
    public Button ReturnButton;
    public Button[] skillButtons = new Button[2];
    public List<Text> parameters = new List<Text>();

    /*　生成したキャラクターを格納する
     *　Dic<キャラクター名,オブジェクト>
     */
    public Dictionary<string, GameObject> charaObjects = new Dictionary<string, GameObject>();  

    GameObject charaObj;
    bool skillWindowFlg = false;

    private void Start()
    {
        characterWindow.SetActive(false);
        skillWindow.SetActive(false);
    }

    private void Update()
    {
        if(skillWindowFlg && Input.GetMouseButtonDown(0))
        {
            skillWindow.SetActive(false);
        }
    }

    /*キャラクターボタンを押した際の処理
     * 生成したキャラクターを消さずに非表示にする
     */
    public void OnClick_CharaButton(CharacterSetDate characterDate)
    {
        charaDate = characterDate;
        ReturnButton.interactable = false;

        if(!charaObjects.ContainsKey(charaDate.GetSetName))
        {
            charaObj = Instantiate(characterDate.CharaObj_anim, spawnPoint.transform.position, Quaternion.identity);
            charaObj.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
            charaObjects.Add(charaDate.GetSetName, charaObj);
        }
        else
        {
            charaObj = charaObjects[charaDate.GetSetName];
        }

        ShowCharaDate(characterDate);
        charaObjects[charaDate.GetSetName].SetActive(true);
        characterWindow.SetActive(true);
    }

    //キャラクターデータの表示
    public void ShowCharaDate(CharacterSetDate charaDate)
    {
        parameters[0].text = "名前： " + charaDate.GetSetName;
        parameters[1].text = "HP： " + charaDate.GetSetHP;
        parameters[2].text = "Leve： " + charaDate.GetSetLevel;
        parameters[3].text = "攻撃力： " + charaDate.GetSetSTR;
        parameters[4].text = "防御力： " + charaDate.GetSetDEF;

        skillButtons[0].GetComponent<Image>().sprite = charaDate.GetSetSkill.GetSetSkillSprite;
        skillButtons[1].GetComponent<Image>().sprite = charaDate.GetSetSpecialSkill.GetSetSkillSprite;
    }

    //キャラクター詳細の非表示
    public void CloseWindow(GameObject Obj)
    {
        Obj.SetActive(false);
        charaObj.SetActive(false);
        ReturnButton.interactable = true;
    }

    /*スキル詳細の表示
     *引数　⇒　true で　Skill
     *　　　　　false で　SpecialSkill
     */
    public void OpenSkillWindow(bool skillFlg)
    {
        string skillType;
        skillWindowFlg = true;
        if(skillFlg)
        {
            parameters[5].text = "名前：" + charaDate.GetSetSkill.GetSetSkillName;
            parameters[6].text = "威力：" + charaDate.GetSetSkill.GetSetSkillDamage;
            parameters[7].text = "クールタイム：" + charaDate.GetSetSkill.GetSetCoolTime;

            if (charaDate.GetSetSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.SingleAttack)
            {
                skillType = "単体攻撃";
            }
            else if (charaDate.GetSetSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.MultipleAttack)
            {
                skillType = "範囲攻撃";
            }
            else
            {
                skillType = "回復";
            }
            parameters[8].text = "種類：" + skillType;
        }
        else
        {
            parameters[5].text = "名前：" + charaDate.GetSetSpecialSkill.GetSetSkillName;
            parameters[6].text = "威力：" + charaDate.GetSetSpecialSkill.GetSetSkillDamage;
            parameters[7].text = "クールタイム：" + charaDate.GetSetSpecialSkill.GetSetCoolTime;

            if (charaDate.GetSetSpecialSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.SingleAttack)
            {
                skillType = "単体攻撃";
            }
            else if (charaDate.GetSetSpecialSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.MultipleAttack)
            {
                skillType = "範囲攻撃";
            }
            else
            {
                skillType = "回復";
            }
            parameters[8].text = "種類：" + skillType;
        }
        skillWindow.SetActive(true);
    }
}
