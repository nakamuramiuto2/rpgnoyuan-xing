using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//待機状態の時の処理
public class EnemyState_Idle : StateChild_Enemy
{
    public override void OnEnter()
    {
        StateType = (int)EnemyStateController.StateType.Idle;

        //アニメーション
        anim.SetTrigger("idle");

        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {
        //Nothing
    }

    /*Idle状態の時は状態の変更があるまでStateを変更しない
     * Idle状態を返す
     */

    public override int StateUpdate(int NextState)
    {
        return NextState;
    }
}
