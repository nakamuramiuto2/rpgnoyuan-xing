using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ダメージを受けたときの処理
public class EnemyState_Hurt : StateChild_Enemy
{
    public override void OnEnter()
    {
        StateType = (int)EnemyStateController.StateType.Hurt;

        //アニメーション
        anim.SetTrigger("hurt");

        //damage　は　StateChild＿Enemy　で宣言
        enemyParameters.HP -= damage;
        enemyParameters.hpSlider.value = enemyParameters.HP;
        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {

    }

    /* HPが0以下　Die状態へ
     * HPが0以上　Idle状態へ
     * 遷移する
     */
    public override int StateUpdate(int NextState)
    {
        if (enemyParameters.HP <= 0)
        {
            return (int)EnemyStateController.StateType.Die;
        }
        else
        {
            return (int)EnemyStateController.StateType.Idle;
        }
    }
}
