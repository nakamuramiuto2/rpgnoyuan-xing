using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敵の攻撃の処理
public class EnemyState_Attack : StateChild_Enemy
{
    public override void OnEnter()
    {
        StateType = (int)EnemyStateController.StateType.Attack;

        //Skill_１だった場合
        if (attackType == (int)EnemyController.NextSkill.Skill_1)
        {
            //アニメーション
            anim.SetTrigger("attack");

            //ダメージの計算
            int Damage = DamageCalculation.BasicsPower(enemyParameters.STR, enemyParameters.Skill_1.GetSetSkillDamage);

            if (enemyParameters.Skill_1.GetSetSKILLTYPE == Skill.SKILLTYPE.SingleAttack)
            {
                controller.battleManager.DamageToCharacter_Single(Damage);
            }
            else if (enemyParameters.Skill_1.GetSetSKILLTYPE == Skill.SKILLTYPE.MultipleAttack)
            {
                controller.battleManager.DamageToCharacter_Multipul(enemyParameters.Skill_1.GetSetMaxPosition, Damage);
            }
        }
        //Skill＿２だった場合
        else
        {
            //アニメーション
            anim.SetTrigger("special");

            //ダメージの計算
            int Damage = DamageCalculation.BasicsPower(enemyParameters.STR, enemyParameters.Skill_2.GetSetSkillDamage);

            if (enemyParameters.Skill_2.GetSetSKILLTYPE == Skill.SKILLTYPE.SingleAttack)
            {
                controller.battleManager.DamageToCharacter_Single(Damage);
            }
            else if (enemyParameters.Skill_2.GetSetSKILLTYPE == Skill.SKILLTYPE.MultipleAttack)
            {
                controller.battleManager.DamageToCharacter_Multipul(enemyParameters.Skill_2.GetSetMaxPosition, Damage);
            }
        }

        //SE
        characterSE.PlayEnemySE(EnemyStateController.StateType.Attack, attackType);

        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {
        //Nothing
    }

    //Idle状態への移行
    public override int StateUpdate(int NextState)
    {
        return (int)EnemyStateController.StateType.Idle;
    }
}
