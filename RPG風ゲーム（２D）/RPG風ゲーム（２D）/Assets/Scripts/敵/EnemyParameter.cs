using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//敵のパラメータのコピー先
public class EnemyParameter : MonoBehaviour
{
    [SerializeField] private float hp;　　//ヒットポイント、体力
    [SerializeField] private int str;　　//攻撃力
    [SerializeField] private int def;  //防御力
    [Range(0, 4)]
    public int position;  //自分の場所(技の範囲で使用)
    [SerializeField] private Skill skill_1;
    [SerializeField] private Skill skill_2;

    public StageInfomation stageInfo;
    public Slider hpSlider;

    public float HP
    {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
        }
    }

    public int STR
    {
        get
        {
            return str;
        }
        set
        {
            str = value;
        }
    }

    public int DEF
    {
        get
        {
            return def;
        }
        set
        {
            def = value;
        }
    }

    public int Position
    {
        get
        {
            return position;
        }
        set
        {
            position = value;
        }
    }
    public Skill Skill_1
    {
        get
        {
            return skill_1;
        }
        set
        {
            skill_1 = value;
        }
    }
    public Skill Skill_2
    {
        get
        {
            return skill_2;
        }
        set
        {
            skill_2 = value;
        }
    }

    /*パラメータのセット
     * 引数　⇒　enemyDate - 敵のデータ
     * 　　　　　positionNum - 自身の場所
     */
    public void SetParameter(EnemySetDate enemyDate,int positionNum)
    {
        HP = enemyDate.GetSetHp;
        STR = enemyDate.GetSetStr;
        DEF = enemyDate.GetSetDef;
        Position = positionNum;
        Skill_1 = enemyDate.GetSetSkill_1;
        Skill_2 = enemyDate.GetSetSkill_2;
        hpSlider.maxValue = enemyDate.GetSetHp;
        hpSlider.value = HP;
    }

}
