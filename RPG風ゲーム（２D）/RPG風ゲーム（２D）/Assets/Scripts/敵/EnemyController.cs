using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敵の処理
public class EnemyController : MonoBehaviour
{
    // 次のスキルが何か
    public enum NextSkill 
    {
        Skill_1,
        Skill_2
    }
    public NextSkill nextSkill;

    private float timeCount = 0.0f;
    private  float skillCoolTime = 0;

    //必要なデータ
    public EnemyParameter enemyParameter;
    public EnemyStateChange stateChange;
    public BattleDate battleDate;
    public NewBattleManager battleManager;

    //敵の攻撃は0.8〜1.5をかけてランダム性を出す
    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.5f);
        skillCoolTime = enemyParameter.Skill_1.GetSetCoolTime * Random.Range(0.6f,1.5f);
        nextSkill = NextSkill.Skill_1;
    }

    // Update is called once per frame
    void Update()
    {
        if (battleDate.GetSetbuttleFlg)
        {
            CountCoolTime();
        }
    }

    /*　スキルを発動する処理
     * 　クールタイムになるとSkill_1が発動
     * 　その後　Skill_2に移行・・・
     * 　の繰り返し
     */
    private void CountCoolTime()
    {
        timeCount += Time.deltaTime;

        if(timeCount >= skillCoolTime)
        {
            if (nextSkill == NextSkill.Skill_1)
            {
                stateChange.Attack((int)NextSkill.Skill_1);
                timeCount = 0.0f;
                skillCoolTime = enemyParameter.Skill_2.GetSetCoolTime;
                nextSkill = NextSkill.Skill_2;
            }
            else
            {
                stateChange.Attack((int)NextSkill.Skill_2);
                timeCount = 0.0f;
                skillCoolTime = enemyParameter.Skill_1.GetSetCoolTime;
                nextSkill = NextSkill.Skill_1;
            }
        }
    }

    /*このオブジェクト自体の削除　
     * 削除のタイミングはDieアニメーションが終わったとき
     */
    public void DestroyGameObject()
    {
        battleManager.RemoveEnemyList(enemyParameter.Position);
        Destroy(gameObject);
    }
}
