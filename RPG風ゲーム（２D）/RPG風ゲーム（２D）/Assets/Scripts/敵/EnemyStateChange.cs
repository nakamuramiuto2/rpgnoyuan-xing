using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*敵のStateを変更する情報の受付処理
 * Die状態はHurt状態からの遷移のため
 * 受付はない
 */
public class EnemyStateChange : MonoBehaviour
{
    [SerializeField] private EnemyStateController stateController = default;
    public BattleDate battleDate;
    
    //ダメージを受けた際に生成するテキスト
    public GameObject damageText;

    private Quaternion quaternion = Quaternion.Euler(0f, 0f, 0f);

    //初期化処理
    public void Start()
    {
        stateController.Initialize((int)EnemyStateController.StateType.Idle);
    }

    //Idle状態の受付
    public void Idle()
    {
        stateController.UpdateSequence((int)EnemyStateController.StateType.Idle);
    }

    /*Hurt状態の受付
     * 引数　⇒　受けたダメージ
     */
    public void Damaged(int Damage)
    {
        stateController.DamageSend(Damage);
        stateController.UpdateSequence((int)EnemyStateController.StateType.Hurt);

        //ダメージのテキストの生成
        ObjectPool.instance.GetGameObject(damageText, gameObject.transform.position + new Vector3(0f, 2.0f, 0f), quaternion, Damage);
    }

    /*Attack状態の受付
     * 引数　⇒　攻撃の種類（Skill_1 か skill_2）
     */
    public void Attack(int SkillType)
    {
        stateController.AttackTypeSend(SkillType);
        stateController.UpdateSequence((int)EnemyStateController.StateType.Attack);
    }
}
