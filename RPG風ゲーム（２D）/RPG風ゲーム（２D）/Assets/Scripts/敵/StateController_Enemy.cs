using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敵のStateを更新する処理
public abstract class StateController_Enemy : MonoBehaviour
{
    //Stateを辞書に登録
    protected Dictionary<int, StateChild_Enemy> StateDic = new Dictionary<int, StateChild_Enemy>();
    public NewBattleManager battleManager;

    //現在のState
    public int CurrentState { protected set; get; }

    //初期化
    public abstract void Initialize(int initializeStateType);

    /*Stateの更新
     * 引数　⇒　次のState
     */
    public void UpdateSequence(int NextState)
    {
        int nextState = (int)StateDic[CurrentState].StateUpdate(NextState);
        AutoStateTransitionSequence(nextState);
    }

    //Stateの更新時の各Stateの処理
    protected void AutoStateTransitionSequence(int nextState)
    {
        if (CurrentState == nextState)
        {
            return;
        }

        StateDic[CurrentState].OnExit();
        CurrentState = nextState;
        StateDic[CurrentState].OnEnter();
    }

    //受けたダメージを　Hurt　State　に登録
    public void DamageSend(int Damage)
    {
        StateDic[(int)EnemyStateController.StateType.Hurt].damage = Damage;
    }

    //与える攻撃の種類を　Attack　State　に登録
    public void AttackTypeSend(int AttackType)
    {
        StateDic[(int)EnemyStateController.StateType.Attack].attackType = AttackType;
    }
}
