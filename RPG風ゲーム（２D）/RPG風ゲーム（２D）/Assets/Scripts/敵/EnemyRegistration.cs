using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敵の情報が必要なScriptへ設定する処理
public class EnemyRegistration : MonoBehaviour
{
    //敵のデータ
    public EnemySetDate enemyDate;
    //敵のポジション
    public int positionNum;
    //バトルを管理する
    public NewBattleManager battleManager;
    //敵のStateを変更する受付
    public EnemyStateChange enemyStateChange;
    //敵のパラメータのコピー
    public EnemyParameter enemyParameter;
    //敵のStateを変更する
    public EnemyController enemyController;

    //それぞれをセットする
    void Start()
    {
        battleManager = GameObject.Find("BattleManager").GetComponent<NewBattleManager>();
        enemyParameter.SetParameter(enemyDate,positionNum);
        battleManager.InEnemyList(positionNum, enemyStateChange);
        enemyController.battleManager = battleManager;
    }
}
