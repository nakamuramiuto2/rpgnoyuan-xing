using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//敵がやられた時の処理
public class EnemyState_Die : StateChild_Enemy
{
    public override void OnEnter()
    {
        StateType = (int)EnemyStateController.StateType.Die;

        //アニメーション
        anim.SetTrigger("die");

        //SE
        characterSE.PlayEnemySE(EnemyStateController.StateType.Die);
    }

    public override void OnExit()
    {

    }

    /*敵がやられるとオブジェクト自体を削除するため
     * この関数は呼ばれない
     * 仮の　０　を返している
     */

    public override int StateUpdate(int NextState)
    {
        return 0;
    }
}
