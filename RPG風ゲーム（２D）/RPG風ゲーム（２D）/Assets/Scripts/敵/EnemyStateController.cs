using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//各Stateの初期化、登録
public class EnemyStateController : StateController_Enemy
{
    //Stateの種類
    public enum StateType
    {
        Idle,
        Attack,
        Hurt,
        Die
    }

    //登録するState
    public EnemyState_Idle _Idle;
    public EnemyState_Attack _Attack;
    public EnemyState_Hurt _Hurt;
    public EnemyState_Die _Die;

    //初期化処理
    public override void Initialize(int initializeStateType)
    {
        battleManager = GameObject.Find("BattleManager").GetComponent<NewBattleManager>();

        //待機（Idle状態）
        StateDic[(int)StateType.Idle] = _Idle;
        StateDic[(int)StateType.Idle].Initialize((int)StateType.Idle);

        //攻撃(Attack状態)
        StateDic[(int)StateType.Attack] = _Attack;
        StateDic[(int)StateType.Attack].Initialize((int)StateType.Attack);

        //ダメージ受けた(Hurt状態)
        StateDic[(int)StateType.Hurt] = _Hurt;
        StateDic[(int)StateType.Hurt].Initialize((int)StateType.Hurt);

        //死（Die状態）
        StateDic[(int)StateType.Die] = _Die;
        StateDic[(int)StateType.Die].Initialize((int)StateType.Die);

        CurrentState = initializeStateType;
        StateDic[CurrentState].OnEnter();
    }
}
