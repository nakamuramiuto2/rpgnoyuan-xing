using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//各Stateの処理の基底クラス
public abstract class StateChild_Enemy : MonoBehaviour
{
    protected StateController_Enemy controller;
    //敵のパラメータのコピー
    public EnemyParameter enemyParameters;
    protected Animator anim;

    //SEのスクリプト
    public CharacterSE characterSE;

    //受けたダメージ
    public int damage;
    //与える攻撃の種類
    public int attackType;

    //Stateの種類
    protected int StateType { get; set; }

    //初期化
    public virtual void Initialize(int stateType)
    {
        StateType = stateType;
        if(controller == null)
        {
            controller = GetComponent<StateController_Enemy>();
        }
        //親オブジェクトのパラメータ
        enemyParameters = transform.root.gameObject.GetComponent<EnemyParameter>();
        anim = transform.root.gameObject.GetComponent<Animator>();
    }

    //Stateに入った時に実行される処理
    public abstract void OnEnter();

    //Stateを出る時に実行される処理
    public abstract void OnExit();

    //Stateの更新時に実行される処理
    public abstract int StateUpdate(int StateType);

}
