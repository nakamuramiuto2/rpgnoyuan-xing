using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*Home画面の処理
 */

public class HomeController : MonoBehaviour
{
    public StageInfomation stageInfo;
    public PlayerDate playerDate;
    public AudioController audioController;

    //プレイヤーのデータ
    public Slider expSlider;
    public Text playerName; 
    public Text playerLevel;
    public Text expLevel;

    //現在の背景
    public Image background;

    //変更する背景のリスト
    public Sprite[] backImage = new Sprite[3];

    //背景の番号
    public int imageNum;

    private void Awake()
    {
        SetDate();
    }

    //プレイヤーデータ、背景画像をセット、背景によってBGMを変更
    private void SetDate()
    {
        expSlider.maxValue = playerDate.levelExp[playerDate.Level + 1];
        expSlider.value = playerDate.CurrentExp;
        playerName.text = playerDate.Name.ToString();
        playerLevel.text = playerDate.Level.ToString();
        expLevel.text = "あと"　+ (expSlider.maxValue - expSlider.value).ToString();

        imageNum = Random.Range(0, 3);
        background.sprite = backImage[imageNum];

        //BGMの変更
        switch(imageNum)
        {
            case 0:
                audioController.bgm = "Home_noon";
                break;
            case 1:
                audioController.bgm = "Home_evening";
                break;
            case 2:
                audioController.bgm = "Home_night";
                break;
            default:
                break;
        }
    }

    /*　Debug用の全ステージ解放・リセット処理
     *　引数　⇒　true で解放
     *　　　　　false　でリセット
     */
    public void AllStageOpenClose(bool OpenFlg)
    {
        if(OpenFlg)
        {
            for (int i = 0; i < stageInfo.clearFlg.Count; i++)
            {
                stageInfo.clearFlg[i] = OpenFlg;
            }
        }
        else
        {
            for (int i = 1; i < stageInfo.clearFlg.Count; i++)
            {
                stageInfo.clearFlg[i] = OpenFlg;
            }
        }

    }
}
