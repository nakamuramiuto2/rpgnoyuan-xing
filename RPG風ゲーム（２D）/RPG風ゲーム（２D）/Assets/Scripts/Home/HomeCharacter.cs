using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HomeCharacter : MonoBehaviour
{
    public HomeController homeController;
    public CharaTextManager textManager;

    //キャラクターのボタン
    public Button charaButton;
    public Button textboxButton;

    //セリフを書くテキスト
    public Text selifText;

    void Start()
    {
        ButtonOnOff("Both", false);
        //前回、Homeを開いた時のセリフをリセット
        textManager.RemoveList();
        ButtonOnOff("Both", true);
        selifText.text = textManager.SendText(true,homeController.imageNum).SerifText;
    }

    //テキストを変える
    public void ChangeText()
    {
        selifText.text = textManager.SendText(false, homeController.imageNum).SerifText;
    }


    /*ボタンのON.OFFの管理
     * ButtonName = charaButton  →　charaButton
     *            = textboxButton　→　textboxButton
     *            = Both　→　両方
     * 
     * OnOff　で true か false　を渡す
     */
    private void ButtonOnOff(string ButtonName,bool OnOff)
    {
        if(OnOff)
        {
            switch(ButtonName)
            {
                case "charaButton":
                    charaButton.interactable = OnOff;
                    break;
                case "textboxButton":
                    textboxButton.interactable = OnOff;
                    break;
                case "Both":
                    charaButton.interactable = OnOff;
                    textboxButton.interactable = OnOff;
                    break;
            }
        }
    }
}