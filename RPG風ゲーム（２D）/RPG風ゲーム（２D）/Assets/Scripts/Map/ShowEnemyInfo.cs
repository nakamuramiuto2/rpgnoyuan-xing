using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

//敵のボタンを押した際の敵の詳細の表示
public class ShowEnemyInfo : MonoBehaviour
{
    public EnemySetDate enemyDate;
    public List<Text> parameterTexts = new List<Text>();
    public StageInfomation stageInfo;

    //もし敵のデータがあれば表示する
    public void ShowEnemyDate(int ButtonNum)
    {
        if(stageInfo.ShowEnemys[ButtonNum].GetSetName != "Null")
        {
            enemyDate = stageInfo.ShowEnemys[ButtonNum];
            parameterTexts[0].text = "名前：" + enemyDate.GetSetName;
            parameterTexts[1].text = "\n概要\n" + enemyDate.GetSetIntroduction;
            for (int i = 2; i < 8; i++)
            {
                parameterTexts[i].text = "";
            }
        }
    }
}
