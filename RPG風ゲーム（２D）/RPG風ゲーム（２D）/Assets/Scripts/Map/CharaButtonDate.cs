using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Map　シーン、キャラクター編成画面のキャラクターのデータを格納、画像変更
public class CharaButtonDate : MonoBehaviour
{
    [SerializeField] private CharacterSetDate CharaDate;
    public BattleDate battleDate;

    public CharacterSetDate CharacterDate
    {
        get
        {
            return CharaDate;
        }
        set
        {
            CharaDate = value;
        }
    }

    void Start()
    {
        GetComponent<Image>().sprite = CharaDate.GetSetCharaSprite;
    }
}