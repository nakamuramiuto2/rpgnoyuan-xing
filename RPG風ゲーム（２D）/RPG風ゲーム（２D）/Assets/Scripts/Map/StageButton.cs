using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//マップのステージの情報
public class StageButton : MonoBehaviour
{
    public StageInfomation stageInfo;
    [Header("このマップの番号")]
    public int MapNum;
    [Header("このステージの番号")]
    public int StageNum;
    [Header("手に入るプレイヤー経験値")]
    public int StageExp;
    [Header("ボス戦があるかどうか")]
    public bool bossFlg;
    [Header("表示する敵")]

    //編成画面を開いたときに表示する敵の情報
    public List<EnemySetDate> EnemyButtons = new List<EnemySetDate>();

    //FirstWave　の敵
    public List<EnemySetDate> EnemyFirstInfo = new List<EnemySetDate>();
    //SecondWave の敵
    public List<EnemySetDate> EnemySecondInfo = new List<EnemySetDate>();
    //ThirdWave　の敵
    public List<EnemySetDate> EnemyThirdInfo = new List<EnemySetDate>();

    public StageManager stageManager;

    //ステージのボタンを押した際の処理
    public void OnClick_StageButton()
    {
        //StageInformation　の情報のリセット
        stageInfo.ShowEnemys.Clear();
        stageInfo.FirstEnemys.Clear();
        stageInfo.SecondEnemys.Clear();
        stageInfo.ThirdEnemys.Clear();

        //StageInformation に現在選択したステージの情報を格納
        for(int i = 0;i < EnemyButtons.Count;i++)
        {
            stageInfo.ShowEnemys.Add(EnemyButtons[i]);
        }
        for(int i = 0;i < EnemyFirstInfo.Count;i++)
        {
            stageInfo.FirstEnemys.Add(EnemyFirstInfo[i]);
        }
        for (int i = 0; i < EnemySecondInfo.Count; i++)
        {
            stageInfo.SecondEnemys.Add(EnemySecondInfo[i]);
        }
        for (int i = 0; i < EnemyThirdInfo.Count; i++)
        {
            stageInfo.ThirdEnemys.Add(EnemyThirdInfo[i]);
        }

        stageInfo.MapNum = MapNum;
        stageInfo.StageNum = StageNum;
        stageInfo.StageExp = StageExp;

        stageInfo.BossFlg = bossFlg;

        stageManager.OpenWindow();
    }
}
