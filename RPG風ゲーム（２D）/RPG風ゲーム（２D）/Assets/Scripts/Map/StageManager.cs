using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Map シーンの処理
public class StageManager : MonoBehaviour
{
    public StageInfomation StageInfo;
    public BattleDate battleDate;
    //ステージのボタンのList
    public List<Button> StageButtons = new List<Button>();

    //編成されているキャラクターのList
    public List<Button> CharaButtons = new List<Button>();

    //編成されている敵のList
    public List<Button> EnemyButtons = new List<Button>();

    //表示するウィンドウ
    public GameObject window;
    public Button returnButton;

    //敵の情報が5個なかった時用の画像
    public Sprite NoneImage;

    // Start is called before the first frame update
    void Start()
    {
        //現在クリアしたステージ + 次のステージ　解放
        for(int i = 0;i < StageInfo.clearFlg.Count;i++)
        {
            StageButtons[i].interactable = false;
            if(StageInfo.clearFlg[i])
            {
                StageButtons[i].interactable = true;
            }
        }

        window.SetActive(false);
    }

   /*編成画面を表示
    * 現在編成されているキャラクターと
    * 選択したステージの敵の　画像を設定
    */
    public void OpenWindow()
    {
        for(int i = 0;i < 5;i++)
        {
            Sprite sprite = battleDate.CurrentCharacter[i].GetSetCharaSprite;
            CharaButtons[i].GetComponent<Image>().sprite = sprite;
        }

        for(int i = 0;i < StageInfo.ShowEnemys.Count;i++)
        {
            Sprite sprite = StageInfo.ShowEnemys[i].GetSetSprite;
            EnemyButtons[i].GetComponent<Image>().sprite = sprite;
        }
        window.SetActive(true);
        returnButton.interactable = false;
    }

    /*編成画面を非表示
     * 敵の画像をリセット
     */
    public void CloseWindow()
    {
        window.SetActive(false);
        returnButton.interactable = true;
        for(int i = 0;i < EnemyButtons.Count;i++)
        {
            EnemyButtons[i].GetComponent<Image>().sprite = NoneImage;
        }
    }
}
