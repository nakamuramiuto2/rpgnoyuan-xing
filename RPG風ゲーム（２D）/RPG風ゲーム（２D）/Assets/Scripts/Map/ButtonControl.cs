using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Map シーンの処理
public class ButtonControl : MonoBehaviour
{
    public BattleDate battleDate;

    //編成画面のオブジェクト
    public GameObject CharacterList;

    //現在の編成されているキャラクターのList
    public List<GameObject> Chara_Buttons = new List<GameObject>();

    //編成可能なキャラクター一覧
    public List<GameObject> Chara_Set_Buttons = new List<GameObject>();

    //表示するテキストのList
    public List<Text> Parameters = new List<Text>();

    //現在おしたキャラクターのボタン
    public int CurrentPushButton = 0;

    //長押し、短押し処理に必要
    Coroutine PressCroutine;
    protected bool isPressDown;
    public float PressTime = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        isPressDown = false;
        CharacterList.SetActive(false);
    }


    /*長押し処理
     * 指定した時間内でカーソルを離すと
     * Ccoroutine をリセット
     */
    public void PointerDown(int ButtonNum)
    {
        CurrentPushButton = ButtonNum;

        if (PressCroutine != null)
        {
            StopCoroutine(CharaChoiceOnOff());
        }

        PressCroutine = StartCoroutine(CharaChoiceOnOff());
    }

    //短押し処理
    public void PointerUp(int ButtonNum)
    {
        CurrentPushButton = ButtonNum;

        if (isPressDown)
        {
            isPressDown = false;
            LookCharaStatus();
        }
    }

    /*長押しした際に呼ばれるコルーチン
     * 指定時間押すとキャラクターリストを表示
     */
    IEnumerator CharaChoiceOnOff()
    {
        isPressDown = true;
        yield return new WaitForSeconds(PressTime);
        if (isPressDown)
        {
            CharacterList.SetActive(true);
        }
        isPressDown = false;
        yield break;
    }

    /*キャラクターリストのキャラを押した時の処理
     * 現在編成されているキャラを押したキャラに
     * 変更する
     * 
     */
    public void OnClick_SetCharacter(int ButtonNum)
    {
        Sprite sprite = Chara_Set_Buttons[ButtonNum].GetComponent<Image>().sprite;
        CharacterSetDate buttonDate = Chara_Set_Buttons[ButtonNum].GetComponent<CharaButtonDate>().CharacterDate;
        battleDate.CurrentCharacter[CurrentPushButton] = buttonDate;
        Chara_Buttons[CurrentPushButton].GetComponent<Image>().sprite = sprite;
        RetuenList();
    }
    
    //キャラクターリストを閉じる
    public void RetuenList()
    {
        CharacterList.SetActive(false);
    }

    /*短押しした際に呼ばれる
     * 編成されているキャラの詳細を表示
     */
    private void LookCharaStatus()
    {

        if (battleDate.CurrentCharacter == null)
        {
            Debug.Log("Not Date");
        }
        else
        {
            Parameters[0].text = "名前：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetName;
            Parameters[1].text = "レベル：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetLevel;
            Parameters[2].text = "最大HP：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetHP;
            Parameters[3].text = "物理攻撃力：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetSTR;
            Parameters[4].text = "魔法攻撃力：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetINT;
            Parameters[5].text = "物理防御力：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetDEF;
            Parameters[6].text = "魔法防御力：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetRES;
            Parameters[7].text = "すばやさ：" + battleDate.CurrentCharacter[CurrentPushButton].GetSetAGI;
        }
    }
}
