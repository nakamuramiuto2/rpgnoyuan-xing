using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//キャラクター、敵のSEをだす処理
public class CharacterSE : MonoBehaviour
{
    //それぞれの状態のSEの名前
    [Header("SE：攻撃")]
    [SerializeField] private string attackSE;
    [Header("SE：スペシャル攻撃")]
    [SerializeField] private string specialSE;
    [Header("SE：死")]
    [SerializeField] private string dieSE;
    [Space(20)]

    //Audio関連
    public AudioManager audioManager;
    public AudioSource audioSource;
    public AudioClip audioClip;


    ///キャラクターの音を出す
    ///キャラクターのState（状態）に応じて鳴らす音を変える
    public void PlayCharacterSE(CharacterStateController.StateType stateType)
    {
        if(stateType == CharacterStateController.StateType.Attack)
        {
            audioClip = audioManager.ToSEClip(attackSE);
        }
        else if(stateType == CharacterStateController.StateType.SpecialAttack)
        {
            audioClip = audioManager.ToSEClip(specialSE);
        }

        audioSource.PlayOneShot(audioClip);
    }

    ///敵の音を出す
    ///敵のState（状態）、攻撃の種類に応じて鳴らす音を変える
    public void PlayEnemySE(EnemyStateController.StateType stateType,int attackType = 0)
    {
        if(stateType == EnemyStateController.StateType.Attack)
        {
            if(attackType == 0)
            {
                audioClip = audioManager.ToSEClip(attackSE);
            }
            else
            {
                audioClip = audioManager.ToSEClip(specialSE);
            }
        }
        else if(stateType == EnemyStateController.StateType.Die)
        {
            audioClip = audioManager.ToSEClip(dieSE);
        }

        audioSource.PlayOneShot(audioClip);
    }
}
