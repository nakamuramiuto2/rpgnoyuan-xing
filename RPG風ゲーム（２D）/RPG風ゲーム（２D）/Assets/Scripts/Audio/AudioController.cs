using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


///シーン内のBGM、SEを管理する
public class AudioController : MonoBehaviour
{
    ///UnityEvent
    ///SEが終わったあとにシーン移動する際に使用
    [Serializable] private class SceneMoveEvents : UnityEvent { }
    [SerializeField] private SceneMoveEvents myEvent = null;

    //Audio関連
    public AudioManager audioManager;
    public AudioClip audioClip;
    public AudioSource audioBGM;
    public AudioSource audioSE;

    //そのシーンのBGMの名前
    [Header("BGMの名前")]
    public string bgm;

    public static AudioController audioController;
    public float timeCount;

    //シーン以降後すぐにシーン移動のボタンを押したときSEがならないことを防ぐためのフラグ
    public bool soundFlg = false;
    //シフラグーン移動のSEが終わったことを判定する
    public bool sceneFlg = false;

    void Awake()
    {
        //シングルトン
        if (audioController == null)
        {
            audioController = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        PlayBGMSound(bgm);
    }

    void Update()
    {
        if(!soundFlg)
        {
            //シーン移動0.5秒は音がならないようにする
            timeCount += Time.deltaTime;

            if(timeCount >= 0.5f)
            {
                soundFlg = true;
            }
        }
        else 
        {          
            if (sceneFlg == true && !audioSE.isPlaying)
            {
                Debug.Log("StartSceneMove");
                myEvent.Invoke();
            }
        }
    }

    //BGMをAudioManagerから取得　再生
    public void PlayBGMSound(string bgmName)
    {
        audioClip = audioManager.ToBGMClip(bgmName);
        audioBGM.clip = audioClip;
        audioBGM.Play();
    }

    //SEをAudioManagerから取得　再生
    public void PlaySESound(string seName)
    {
        audioClip = audioManager.ToSEClip(seName);
        audioSE.PlayOneShot(audioClip);
    }

    //BGMの停止
    public void StopBGMSound()
    {
        audioBGM.Pause();
    }

    //BGMの再生（停止後の続きから）
    public void RestartBGMSound()
    {
        audioBGM.UnPause();
    }

    //シーン移動のSEを取得
    public void SceneMove(string seName)
    {
        if(soundFlg)
        {
            Debug.Log("StartSoundEffect");
            audioClip = audioManager.ToSEClip(seName);
            audioSE.PlayOneShot(audioClip);

            sceneFlg = true;
        }
    }

    //シーン移動関数をEventに入れる（シーン移動関数はそれぞれシーン移動するButtonについています）
    public void SceneMoveEventListener(SceneMove sceneMove)
    {
        if(soundFlg)
        {
            myEvent.AddListener(sceneMove.SceneMoving);
        }
    }

    //シーン移動の際ボタンを押せなくする　（無限に押すとSEが常に鳴り、シーン移動しない）
    public void ButtonIntaractable(Button button)
    {
        if(soundFlg)
        {
            button.interactable = false;
        }
    }
}
