using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class SceneMove : MonoBehaviour
{
    //移動するシーンの名前
    public string sceneName;

    //シーン移動
    public void SceneMoving()
    {
        SceneManager.LoadScene(sceneName);
    }
}
