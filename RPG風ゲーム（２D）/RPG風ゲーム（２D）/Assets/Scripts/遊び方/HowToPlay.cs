using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//遊び方の説明ウィンドウの処理
public class HowToPlay : MonoBehaviour
{
    public BattleDate battleDate;

    //ズームする画像
    public GameObject zoomImage;

    //遊び方が書いてあるObject全て
    public GameObject playWindow;

    bool closeFlg = false;

    //オーディオ管理のデータ
    public AudioManager audioManager;
    public AudioClip audioClip;
    public AudioSource audioSource;

    private void Start()
    {
        zoomImage.SetActive(false);
        playWindow.SetActive(false);
    }

    private void Update()
    {
        if(closeFlg && Input.GetMouseButton(0))
        {
            InActiveObject(zoomImage);
            closeFlg = false;
        }
    }

    /*クリックすると対応する画像が拡大される
     * (その後左クリックで解除)
     */
    public void OnClick_Hint(Sprite hintSprite)
    {
        zoomImage.GetComponent<Image>().sprite = hintSprite;
        ActiveObject(zoomImage);
        closeFlg = true;
    }

    //オブジェクトを表示
    public void ActiveObject(GameObject Obj)
    {
        Obj.SetActive(true);
        battleDate.GetSetbuttleFlg = false;
    }

    //オブジェクトを非表示
    public void InActiveObject(GameObject　Obj)
    {
        Obj.SetActive(false);
        battleDate.GetSetbuttleFlg = true;
    }

    ///ボタンを押した際の音を鳴らす
    ///stringで渡した名前の音が鳴る
    public void PlaySESound(string seName)
    {
        audioClip = audioManager.ToSEClip(seName);
        audioSource.PlayOneShot(audioClip);
    }
}
