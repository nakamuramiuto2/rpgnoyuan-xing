using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*キャラクターの状態をStateパターンを使用し管理
 * このScriptはStateの更新をする処理
 */
public abstract class StateController : MonoBehaviour
{
    //Stateを格納する辞書
    protected Dictionary<int, StateChild> StateDic = new Dictionary<int, StateChild>();
    public NewBattleManager battleManager;

    //現在のState
    public int CurrentState { protected set; get; }

    //初期化処理
    public abstract void Initialize(int initializeStateType);

    /*Stateの更新
     * 引数　⇒　次のState
     */
    public void UpdateSequence(int NextState)
    {
        //Debug.Log(NextState + " : " + CurrentState);
        int nextState = (int)StateDic[CurrentState].StateUpdate(NextState);
        AutoStateTransitionSequence(nextState);
    }

    //Stateの更新の際の各Stateの処理
    protected void AutoStateTransitionSequence(int nextState)
    {
        if (CurrentState == nextState)
        {
            return;
        }

        StateDic[CurrentState].OnExit();
        CurrentState = nextState;
        StateDic[CurrentState].OnEnter();
    }
    //受けたダメージを自身の　Hurt Stateに格納
    public void DamageSend(int Damage)
    {
        StateDic[(int)CharacterStateController.StateType.Hurt].damage = Damage;
    }
}