using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*キャラクターの必殺技の処理
 * 基本はAttack状態と変わらない
 */
public class CharaState_SpecialAttack : StateChild
{
    public override void OnEnter()
    {
        StateType = (int)CharacterStateController.StateType.SpecialAttack;

        //SE
        characterSE.PlayCharacterSE(CharacterStateController.StateType.SpecialAttack);

        //アニメーション
        anim.SetTrigger("special");

        int Damage = DamageCalculation.BasicsPower(CharaParameters.STR, CharaParameters.SpecialSkill.GetSetSkillDamage);

        if (CharaParameters.SpecialSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.SingleAttack)
        {
            controller.battleManager.DamageToEnemy_Single(Damage);
        }
        else if (CharaParameters.SpecialSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.MultipleAttack)
        {
            controller.battleManager.DamageToEnemy_Multipul(CharaParameters.NomalSkill.GetSetMaxPosition, Damage);
        }

        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {
        
    }

    public override int StateUpdate(int NextState)
    {
        return (int)CharacterStateController.StateType.Idle;
    }
}
