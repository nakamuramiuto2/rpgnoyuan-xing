using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*キャラクターのStateを変更する情報の受付をする処理
 * Die状態はHurt状態からの遷移のため
 * Die状態の受付はない
 */
public class CharaStateChange : MonoBehaviour
{
    [SerializeField] private CharacterStateController stateController = default;

    //ダメージを受けた際に生成するテキスト
    public GameObject damageText;

    //はじめはIdle状態に
    public void Start()
    {
        stateController.Initialize((int)CharacterStateController.StateType.Idle);
    }

    //Idle状態の受付
    public void Idle()
    {
        stateController.UpdateSequence((int)CharacterStateController.StateType.Idle);
    }

    /*Hurt状態の受付
     * 引数　⇒　受けたダメージ
     */
    public void Damaged(int Damage)
    {
        stateController.DamageSend(Damage);
        stateController.UpdateSequence((int)CharacterStateController.StateType.Hurt);

        //ダメージを表示するテキストの生成
        ObjectPool.instance.GetGameObject(damageText, gameObject.transform.position + new Vector3(0f,2.0f,0f), gameObject.transform.rotation,Damage);
    }

    //Attack状態の受付
    public void Attack()
    {
        stateController.UpdateSequence((int)CharacterStateController.StateType.Attack);
    }

    //Special状態の受付
    public void SpecialAttack()
    {
        stateController.UpdateSequence((int)CharacterStateController.StateType.SpecialAttack);
    }

    //Run状態の受付
    public void Run()
    {
        stateController.UpdateSequence((int)CharacterStateController.StateType.Run);
    }
}
