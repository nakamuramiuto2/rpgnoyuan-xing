using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//キャラクターの攻撃処理
public class CharaState_Attack : StateChild
{
    public override void OnEnter()
    {
        StateType = (int)CharacterStateController.StateType.Attack;
        //SE
        characterSE.PlayCharacterSE(CharacterStateController.StateType.Attack);
        //アニメーション
        anim.SetTrigger("attack");

        //敵に与えるダメージを計算
        int Damage = DamageCalculation.BasicsPower(CharaParameters.STR, CharaParameters.NomalSkill.GetSetSkillDamage);

        /*  battleManagerでどの敵に攻撃を与えるか判断するため
         *　単体攻撃、範囲攻撃によって処理を変える
         */
        if (CharaParameters.NomalSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.SingleAttack)
        {
            controller.battleManager.DamageToEnemy_Single(Damage);
        }
        else if (CharaParameters.NomalSkill.GetSetSKILLTYPE == Skill.SKILLTYPE.MultipleAttack)
        {
            controller.battleManager.DamageToEnemy_Multipul(CharaParameters.NomalSkill.GetSetMaxPosition, Damage);
        }

        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {
        
    }

    //処理が終わったらIdle状態へ
    public override int StateUpdate(int NextState)
    {
        return (int)CharacterStateController.StateType.Idle;
    }
}
