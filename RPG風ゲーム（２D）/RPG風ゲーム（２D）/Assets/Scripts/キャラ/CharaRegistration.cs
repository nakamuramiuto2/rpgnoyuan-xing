using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//キャラクターの必要なデータの登録
public class CharaRegistration : MonoBehaviour
{
    //キャラクターのボタンの番号
    public int ButtonNum;
    //バトル進行の処理
    public NewBattleManager battleManager;
    //バトルに必要なデータ
    public BattleDate battleDate;
    //キャラクターのStatを変える受付
    public CharaStateChange charaStateChange;
    //キャラクターのパラメータ
    public NewCharacterParameters charaParameter;
    //キャラクターボタン
    public SpecialButtonControl specialButtonControl;

    //それぞれを設定
    void Start()
    {
        battleManager = GameObject.Find("BattleManager").GetComponent<NewBattleManager>();
        specialButtonControl = GameObject.Find("SpecialButtons").GetComponent<SpecialButtonControl>();
        charaParameter.SetChara(ButtonNum);
        battleManager.InCharacterList(ButtonNum, charaStateChange);
        specialButtonControl.SpecialButtons[ButtonNum].stateChange = charaStateChange;
        specialButtonControl.SpecialButtons[ButtonNum].character = charaParameter;
    }

    //やられた際のオブジェクトの削除、ボタンを押せなくする
    public void DestroyGameObject()
    {
        battleManager.RemoveCharaList(charaParameter.Position);
        specialButtonControl.SpecialButtons[ButtonNum].ButtonOnOff("Both", false);
        Destroy(transform.root.gameObject);
    }
}
