using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//キャラクターがダメージを受けたときの処理
public class CharaState_Hurt : StateChild
{
    public override void OnEnter()
    {
        StateType = (int)CharacterStateController.StateType.Hurt;

        //アニメーション
        anim.SetTrigger("hurt");

        // damage はStateChild　で宣言
        CharaParameters.HP -= damage;
        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {

    }

    /*　HPが0以下　⇒　Die状態に
     *　0以上　⇒　Idle状態に
     *　移行
     */
    public override int StateUpdate(int NextState)
    {
        if(CharaParameters.HP <= 0)
        {
            return (int)CharacterStateController.StateType.Die;
        }
        else
        {
            return (int)CharacterStateController.StateType.Idle;
        }

    }
}
