using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//各Stateの処理の基底クラス
public abstract class StateChild : MonoBehaviour
{
    protected StateController controller;
    //キャラクターのステータスのコピー先
    public NewCharacterParameters CharaParameters;

    //SEのスクリプト
    public CharacterSE characterSE;

    protected Animator anim;

    //受けたダメージ
    public int damage;
    
    //自身のState
    protected int StateType { get; set; }

    //初期化
    public virtual void Initialize(int stateType)
    {
        StateType = stateType;
        controller = GetComponent<StateController>();
        //自身（親）の parameter を取得
        if(CharaParameters == null)
        {
            CharaParameters = transform.root.gameObject.GetComponent<NewCharacterParameters>();
        }
        anim = transform.root.gameObject.GetComponent<Animator>();
    }

    //Stateに入った時の処理
    public abstract void OnEnter();
    //Stateを出る時の処理
    public abstract void OnExit();
    //Stateを更新する時の処理
    public abstract int StateUpdate(int StateType);
}
