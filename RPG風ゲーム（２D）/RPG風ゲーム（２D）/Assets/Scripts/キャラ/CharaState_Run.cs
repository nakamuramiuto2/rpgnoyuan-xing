using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//キャラクターが走る時の処理
public class CharaState_Run : StateChild
{
    public Rigidbody2D rb;
    //敵を倒した際の移動速度
    public float speed = 0.0f;

    // x軸　正の方向へ止まるポイントに行くまで移動
    public override void OnEnter()
    {
        StateType = (int)CharacterStateController.StateType.Run;

        //アニメーション
        anim.SetBool("run", true);

        if (rb == null)
        {
            rb = transform.root.GetComponent<Rigidbody2D>();
        }

        rb.velocity = new Vector2(speed, 0);
        controller.UpdateSequence(StateType);
    }

    //Run状態を出る時はvelocityを0に戻す
    public override void OnExit()
    {
        rb.velocity = Vector2.zero;
        anim.SetBool("run", false);
    }

    //Idle状態が引数に来るまでRun状態を維持
    public override int StateUpdate(int NextState)
    {
        if(NextState == (int)CharacterStateController.StateType.Idle)
        {
            return (int)CharacterStateController.StateType.Idle;
        }

        return (int)CharacterStateController.StateType.Run;
    }
}