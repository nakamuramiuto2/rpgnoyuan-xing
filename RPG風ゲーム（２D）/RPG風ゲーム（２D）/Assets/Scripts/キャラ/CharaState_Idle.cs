using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//キャラクターの待機時の処理
public class CharaState_Idle : StateChild
{
    public override void OnEnter()
    {
        StateType = (int)CharacterStateController.StateType.Idle;

        //アニメーション
        anim.SetTrigger("idle");

        controller.UpdateSequence(StateType);
    }

    public override void OnExit()
    {
        //Nothing
    }

    /*Idle状態の時はStateの変更があるまでIdle状態をキープするので
     * Idleを返す
     */
    public override int StateUpdate(int NextState)
    {
        return NextState;
    }
}
