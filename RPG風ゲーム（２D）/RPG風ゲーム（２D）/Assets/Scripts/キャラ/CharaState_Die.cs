using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//キャラクターが倒された時の処理
public class CharaState_Die : StateChild
{
    public override void OnEnter()
    {
        StateType = (int)CharacterStateController.StateType.Die;

        //アニメーション
        anim.SetTrigger("die");
    }

    public override void OnExit()
    {

    }

    /*　倒されるとDestroyするためこの関数は呼ばれない
     * 　⇒　仮で 0 を return　している
     */
    public override int StateUpdate(int NextState)
    {
        return 0;
    }
}
