using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*　キャラクターのStateの初期化処理
 */
public class CharacterStateController : StateController
{
    //Stateの種類
    public enum StateType
    {
        Idle,
        Attack,
        SpecialAttack,
        Hurt,
        Run,
        Die
    }

    //各Stateに登録するScript
    public CharaState_Idle _Idle;
    public CharaState_Attack _Attack;
    public CharaState_SpecialAttack _SpecialAttack;
    public CharaState_Hurt _Hurt;
    public CharaState_Run _Run;
    public CharaState_Die _Die;

    /*初期化処理
     * 各StateにScriptを登録する
     */
    public override void Initialize(int initializeStateType)
    {
        battleManager = GameObject.Find("BattleManager").GetComponent<NewBattleManager>();

        //待機（Idle状態）
        StateDic[(int)StateType.Idle] = _Idle;
        StateDic[(int)StateType.Idle].Initialize((int)StateType.Idle);

        //攻撃(Attack状態)
        StateDic[(int)StateType.Attack] = _Attack;
        StateDic[(int)StateType.Attack].Initialize((int)StateType.Attack);

        //スペシャルアタック(Special状態)
        StateDic[(int)StateType.SpecialAttack] = _SpecialAttack;
        StateDic[(int)StateType.SpecialAttack].Initialize((int)StateType.SpecialAttack);

        //ダメージ受けた(Hurt状態)
        StateDic[(int)StateType.Hurt] = _Hurt;
        StateDic[(int)StateType.Hurt].Initialize((int)StateType.Hurt);

        //走る（Run状態）
        StateDic[(int)StateType.Run] = _Run;
        StateDic[(int)StateType.Run].Initialize((int)StateType.Run);

        //死（Die状態）
        StateDic[(int)StateType.Die] = _Die;
        StateDic[(int)StateType.Die].Initialize((int)StateType.Die);

        CurrentState = initializeStateType;
        StateDic[CurrentState].OnEnter();
    }
}